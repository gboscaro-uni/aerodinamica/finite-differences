function [ x ] = thomas( A )

    %A matrice in forma compatta 
    %TODO controlli A compatta ( e m = 4)
    dim = size(A);
    n = dim(1);
    m = dim(2);
    P = zeros(m,n);
    
    %inizializzo prima riga
    P(1,1) = A(1,1); %coefficiente a
    P(1,2) = A(1,2); %coefficiente b
    P(1,4) = A(1,4)/P(1,2); %coefficiente f
    P(1,3) = A(1,3)/P(1,2); %coefficiente c
    
    %inizializzo il resto della matrice compatta corretta
    for i = 2:1:n
        P(i,1) = A(i,1); %coefficiente a
        P(i,2) = A(i,2)-A(i,1)*(A(i-1,3)/P(i-1,2)); %coefficiente b
        P(i,4) = (A(i,4)-(A(i,1)*P(i-1,4)))/P(i,2); %coefficiente f
        P(i,3) = A(i,3)/P(i,2); %coefficiente c
    end
    
    %risolvo 
    x = zeros(n,1);
    x(n) = P(n,4);
    k = n - 1;
    while k > 0
        x(k) = P(k,4) - x(k+1)*P(k,3);
        k = k - 1;
    end
end