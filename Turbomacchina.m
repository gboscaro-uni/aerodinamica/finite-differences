classdef Turbomacchina < handle

   properties (SetAccess = 'private', GetAccess = 'private')
      xa; ya;
      xb; yb;
      xc; yc;
      xd; yd;
      xg; yg; rg;
      xf; yf; rf;
      xp; yp;
      M; N;
      interp;
      pos45; counter;
   end   
   
   properties (SetAccess = 'public', GetAccess = 'public')    
   end
    
    methods (Access = public)
        function obj = Turbomacchina(pa,pb,pc,pd,pg,pf)
            
            obj.xa = pa(1); obj.ya = pa(2);
            obj.xb = pb(1); obj.yb = pb(2);
            obj.xc = pc(1); obj.yc = pc(2);
            obj.xd = pd(1); obj.yd = pd(2);
            obj.xg = pg(1); obj.yg = pg(2); obj.rg = pg(3);
            obj.xf = pf(1); obj.yf = pf(2); obj.rf = pf(3);
            obj.counter = 0;
           
            obj.solve_eqn();
            
        end
        
        function init(obj)
            figure(1); 
            hold on;
            %limiti assi
            xmax=obj.xb+10; ymax=obj.yb+10;
            xmin=obj.xa-10; ymin=obj.ya-10;
            axis([xmin xmax ymin ymax]);
            title('Differenze Divise');
            xlabel('z'); ylabel('r');
            %plot
            obj.draw_label();
            obj.draw_segment();
            obj.draw_circle();
            hold off;
        end
        function obj = init_nodes(obj,dr,dz)
            [obj.M, obj.counter] = init_nodi(dr,dz,obj.xa,obj.xb,obj.ya,obj.yb,obj.yd,obj.xg,obj.yg,obj.rg,obj.xf,obj.yf,obj.rf,obj.xp,obj.yp,obj.xc,obj.yc);
        end
        function obj = calc_diff(obj,dr,dz,tol)
            obj.M = differenze_divise( dr,dz,obj.ya,tol,obj.M, obj.counter, obj.xc );
        end
        function obj = interpolate(obj,dr,dz)
            v = obj.calc_tg45();
            temp = obj.xa:dr:v(1); obj.pos45 = length(temp);
            [obj.interp,obj.N] = interpola( dr,dz,obj.M,obj.pos45 );
        end
        function obj = calc_flusso(obj,dr,dz,flux,tol)
            [rz,rr] = flusso( obj.M, obj.N,flux, dr,dz, obj.pos45, tol );
            obj.draw_flux(rz,rr);
        end
        %funzioni di debugging
        function show_grid_rot(obj)
            for i = 1:1:length(obj.N)
                for j = 1:1:length(obj.N{i})
                    fprintf('r: %d z: %d psi: %d \n',obj.N{i}{j}.getY(),obj.N{i}{j}.getX(),obj.N{i}{j}.getPsi());
                end
                fprintf('\n -------------- \n');
            end
        end
        function show_grid(obj)
            for i=1:1:length(obj.M)
                for j=1:1:length(obj.M{i})
                    fprintf('z: %d r: %d psi: %d \n',obj.M{i}{j}.getX(),obj.M{i}{j}.getY(),obj.M{i}{j}.getPsi());
                end
                fprintf('\n -------------- \n');
            end
        end
        function show_lambda(obj)
            for i=1:1:length(obj.M)
                for j=1:1:length(obj.M{i})
                    vl = obj.M{i}{j}.getLambda(); l1 = vl(1); l2 = vl(2); l3 = vl(3); l4 = vl(4);
                    fprintf('z: %d r: %d l1: %1.4f l2: %1.4f l3: %1.4f l4: %1.4f \n',obj.M{i}{j}.getX(),obj.M{i}{j}.getY(),l1,l2,l3,l4);
                end
                fprintf('\n -------------- \n');
            end
        end
        function show_psi(obj)
            for i=1:1:length(obj.M)
                for j=1:1:length(obj.M{i})
                    fprintf('\n z: %d r: %d psi: %2.4f psidx: %2.4f psiup: %2.4f psisx: %2.4f psidown: %2.4f \n',obj.M{i}{j}.getX(),obj.M{i}{j}.getY(),obj.M{i}{j}.getPsi(),obj.M{i}{j}.getPsiDx(),obj.M{i}{j}.getPsiUp(),obj.M{i}{j}.getPsiSx(),obj.M{i}{j}.getPsiDown());
                end
                fprintf('\n -------------- \n');
            end
        end
        function show_interp(obj)
            intz = obj.interp{1}; intr = obj.interp{2};
            fprintf('\n --- Prima parte asse z --- \n');
            for i=1:1:length(intz)
                for j=1:1:length(intz{i})
                    fprintf('%f \t',intz{i}(j));
                end
                fprintf('\n');
            end
            fprintf('\n --- Seconda parte asse r --- \n');
            for i=1:1:length(intr)
                for j=1:1:length(intr{i})
                    fprintf('%f \t',intr{i}(j));
                end
                fprintf('\n');
            end
        end
        function show_tg45(obj,v)
            figure(1);
            hold on;
            plot(v(1), v(2), 'bo');
            hold off;
        end
        function show_nodes(obj)
            obj.draw_nodes();
        end
    end
    
    methods (Access = private)
        function draw_label(obj)
            %nomi punti
            plot(obj.xa, obj.ya, 'ko'); text(obj.xa-5,obj.ya-3,'A');
            plot(obj.xb, obj.yb, 'ko'); text(obj.xb+3,obj.yb+3,'B');
            plot(obj.xc, obj.yc, 'ko'); text(obj.xc-5,obj.yc+3,'C');
            plot(obj.xd, obj.yd, 'ko'); text(obj.xd-5,obj.yd+3,'D');
            plot(obj.xf, obj.yf-obj.rf, 'ko'); text(obj.xf-5,obj.yf-obj.rf+3,'O');

            plot(obj.xg, obj.yg-obj.rg, 'ko'); text(obj.xg+3,obj.yg-obj.rg-3,'M');
            plot(obj.xg+obj.rg, obj.yg, 'ko'); text(obj.xg+obj.rg+3,obj.yg-3,'N');
            
            plot(obj.xp, obj.yp, 'ko'); text(obj.xp-5,obj.yp+3,'P');
            
            plot(obj.xg, obj.yg, 'k*'); text(obj.xg-5,obj.yg+3,'G');
            plot(obj.xf, obj.yf, 'k*'); text(obj.xf-5,obj.yf+3,'F');
        end
        function draw_segment(obj)
            %segmenti
            plot([obj.xa,obj.xd],[obj.ya,obj.yd],'r-');
            plot([obj.xb,obj.xc],[obj.yb,obj.yc],'r-');
            plot([obj.xa,obj.xg],[obj.ya,obj.yg-obj.rg],'r-');
            plot([obj.xb,obj.xg+obj.rg],[obj.yb,obj.yg],'r-');
            plot([obj.xd,obj.xf],[obj.yd,obj.yf-obj.rf],'r-');
            plot([obj.xc,obj.xp],[obj.yc,obj.yp],'r-');
        end
        function draw_circle(obj)
            % circonferenze
            a=3*pi/2; b=0.0001;
            PG=draw_circle(a,b,obj.xg,obj.yg,obj.rg,2*pi);
            plot(PG(:,1),PG(:,2),'r-');

            a=3*pi/2; b=0.0001; t=a+atan((obj.xp-obj.xf)/(obj.yf-obj.yp));
            PF=draw_circle(a,b,obj.xf,obj.yf,obj.rf,t);
            plot(PF(:,1),PF(:,2),'r-');
        end
        function obj = solve_eqn(obj)
            CF=sqrt((obj.xc-obj.xf)^2+(obj.yc-obj.yf)^2); %segmento CF
            delta=(obj.xf*(CF^2-obj.rf^2)+obj.xc*obj.rf^2)^2-(CF^2)*((obj.xf^2-obj.rf^2)*(CF^2-obj.rf^2)+obj.xc^2*obj.rf^2); %delta equazione
            %soluzioni x
            xP1=(obj.xf*(CF^2-obj.rf^2)+obj.xc*obj.rf^2)/(CF^2)+(sqrt(delta))/(CF^2);
            xP2=(obj.xf*(CF^2-obj.rf^2)+obj.xc*obj.rf^2)/(CF^2)-(sqrt(delta))/(CF^2);
            if xP1>obj.xf
                obj.xp=xP1;
            else
                obj.yp=xP2;
            end
            %soluzioni y
            yP1=obj.yf+sqrt(obj.rf^2-(obj.xp-obj.xf)^2);
            yP2=obj.yf-sqrt(obj.rf^2-(obj.xp-obj.xf)^2);
            if yP1<obj.yf
                obj.yp=yP1;    
            else
                obj.yp=yP2;    
            end
        end
        function draw_nodes(obj)
            figure(1); 
            hold on;
            for i=1:1:length(obj.M)
                for j=1:1:length(obj.M{i})
                    p=obj.M{i}{j}.getPsi();
                    if p < 0.1
                        color = '.y';
                    elseif p < 0.2
                        color = '.y';
                    elseif p < 0.3
                        color = '.m';
                    elseif p < 0.4
                        color = '.m';
                    elseif p < 0.5
                        color = '.r';
                    elseif p < 0.6
                        color = '.g';
                    elseif p < 0.7
                        color = '.c';
                    elseif p < 0.8
                        color = '.b';
                    elseif p < 0.9
                        color = '.k';
                    else
                        color = '.k';
                    end
                    plot(obj.M{i}{j}.getX(),obj.M{i}{j}.getY(),color);
                end
            end
            hold off;
        end
        function draw_flux(obj,vetz,vetr)
           figure(1); 
           hold on;
           plot( [obj.xa,vetz(1,1)] , [vetz(1,2),vetz(1,2)] ,'b-');
           %riferimento asse z
           for i = 2:1:length(vetz)
               plot( [vetz(i-1,1),vetz(i,1)] , [vetz(i-1,2),vetz(i,2)] ,'b-');
           end
           plot([vetz(i,1),vetr(1,2)] , [vetz(i,2),vetr(1,1)],'b-');
           %riferimento asse r
           for i = 2:1:length(vetr)
               plot( [vetr(i-1,2),vetr(i,2)] , [vetr(i-1,1),vetr(i,1)] ,'b-');
           end
           plot( [vetr(i,2),vetr(i,2)] , [vetr(i,1),obj.yb] ,'b-');
           
           hold off;
        end
        function r = calc_tg45(obj)
           x = obj.xf + obj.rf*sqrt(2)/2;
           y = obj.yf - obj.rf*sqrt(2)/2;
           r = [x,y];
        end
        
    end
    
end