clear all;
%coordinate punti notevoli
xa=10; ya=7;
xb=99; yb=100;
xc=89; yc=100;
xd=10; yd=39.5;
xg=51.5; yg=54.5; rg=47.5;
xf=64; yf=59.5; rf=20;

dr=1.5; dz=1.5;
tol=1e-4;
percentuale_flusso = 0.5;

turbomacchina = Turbomacchina([xa,ya],[xb,yb],[xc,yc],[xd,yd],[xg,yg,rg],[xf,yf,rf]);
turbomacchina.init();
turbomacchina.init_nodes(dr,dz);
turbomacchina.calc_diff(dr,dz,tol);
turbomacchina.interpolate(dr,dz);
turbomacchina.calc_flusso(dr,dz,percentuale_flusso,tol);

%debug
%turbomacchina.show_grid();
%turbomacchina.show_lambda();
%turbomacchina.show_interp();
%turbomacchina.show_grid_rot();
%turbomacchina.show_nodes();
%turbomacchina.show_psi();