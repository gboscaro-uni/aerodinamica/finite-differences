function [ r,N ] = interpola( dr,dz,M, p45 )

    fprintf('\n --- Interpolazione ---');
    startingTime = cputime;

    M0 = 0;
    MN = 0;
    
    tg = p45;
    
    n = length(M);
    vetz = cell(tg);
    %seconda parte
    
    %riferimento asse z
    for iz = 1:1:tg
        m = length(M{iz});
        P = zeros(m,4);
        
        P(1,1) = 0;
        P(1,2) = (2*dr)/3;
        P(1,3) = dr/6;
        P(1,4) = (M{iz}{2}.getPsi()-M{iz}{1}.getPsi())/dr - (M{iz}{1}.getPsi()-M{iz}{1}.getPsiDown())/dr - dr*M0/6;
            
        for j = 2:1:m-1
            P(j,1) = dr/6;
            P(j,2) = (2*dr)/3;
            P(j,3) = dr/6;
            P(j,4) = (M{iz}{j+1}.getPsi()-M{iz}{j}.getPsi())/dr - (M{iz}{j}.getPsi()-M{iz}{j-1}.getPsi())/dr;
        end
        
        P(m,1) = dr/6;
        P(m,2) = (2*dr)/3;
        P(m,3) = 0;
        P(m,4) = (M{iz}{m}.getPsiUp()-M{iz}{m}.getPsi())/dr - (M{iz}{m}.getPsi()-M{iz}{m-1}.getPsi())/dr - dr*MN/6;
        
        x = thomas(P);
        vetz{iz} = x;
        for h = 1:1:m
            M{iz}{h}.setM(x(h));
        end
    end
    
    %riferimento asse r
    
    N = rotate_ref( M,dr,dz,p45-1 );
    n = length(N);
    vetr = cell(n);
    
    for ir = 1:1:n
        m = length(N{ir});
        P = zeros(m,4);
        P(1,1) = 0;
        P(1,2) = (2*dz)/3;
        P(1,3) = dz/6;
        P(1,4) = (N{ir}{1}.getPsiSx()-N{ir}{1}.getPsi())/dz - (N{ir}{1}.getPsi()-N{ir}{1}.getPsiDx())/dz - dz*M0/6;

        for j = 2:1:m-1
            P(j,1) = dz/6;
            P(j,2) = (2*dz)/3;
            P(j,3) = dz/6;
            P(j,4) = (N{ir}{j}.getPsiSx()-N{ir}{j}.getPsi())/dz - (N{ir}{j}.getPsi()-N{ir}{j}.getPsiDx())/dz;
        end

        P(m,1) = dz/6;
        P(m,2) = (2*dz)/3;
        P(m,3) = 0;
        P(m,4) = (N{ir}{m}.getPsiSx()-N{ir}{m}.getPsi())/dz - (N{ir}{m}.getPsi()-N{ir}{m}.getPsiDx())/dz - dz*MN/6;

        x = thomas(P);
        vetr{ir} = x;
        for h = 1:1:m
            N{ir}{h}.setM(x(h));
        end
    end
    
    r = {vetz,vetr};
    
    endTime = cputime - startingTime;
    fprintf('\n --- Interpolazione completata in %f secondi ---', endTime);

end