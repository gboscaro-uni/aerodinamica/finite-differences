function [ py_prev,py_next ] = check_y( prev,y,next,py )

    py_prev = 0; py_next = 0; 
    n = length(next);
    if(n > 0)
        for i = 1:1:n
            if(next{i}.getY() == y)
                py_next = i;
            end
        end
    end
    m = length(prev);
    if(m > 0)
        for j = 1:1:m
            if(prev{j}.getY() == y)
                py_prev = j;
            end
        end
    end

end

