function [ rz,rr ] = flusso( M, N,flux, dr,dz, p45, tol )

    fprintf('\n --- Calcolo linea di flusso ---');
    startingTime = cputime;
    
    rz = zeros(p45,2);
    %asse z
    for z = 1:1:p45
        m = length(M{z});
        for r = 1:1:m
            psik = M{z}{r}.getPsi();
            if(psik > 0.5) %TODO caso particolare == 0.5
                
                rk = M{z}{r}.getY();
                Mk = M{z}{r}.getM();
                rprev = M{z}{r-1}.getY();
                psiprev = M{z}{r-1}.getPsi(); %TO FIX (down??)
                Mprev = M{z}{r-1}.getM();
                
                rm = (rk + rprev)/2;
                
                Fr = @(r) [ (Mprev/(6*dz))*(rk-r)^3 + (Mk/(6*dz))*(r-rprev)^3 + (psik/dz - (Mk*dz)/6)*(r - rprev) + (psiprev/dz - (Mprev*dz)/6)*(rk-r) - flux];
                dFr = @(r) [ (-Mprev/(2*dz))*(rk-r)^2 + (Mk/(2*dz))*(r-rprev)^2 + (psik/dz - (Mk*dz)/6) - (psiprev/dz - (Mprev*dz)/6) ];
    
                [res,~,~] = newton(Fr,dFr,rm,tol,1000);
                
                rz(z,1) = M{z}{r}.getX();
                rz(z,2) = res(end);
                break;
            end
        end
    end
    %asse r
    rr = [];
    for z = 1:1:length(N)
        m = length(N{z});
        for r = 1:1:m
            psik = N{z}{r}.getPsi();
            if(psik > 0.5) %TODO caso particolare == 0.5
                
                rk = 99-N{z}{r}.getX();
                Mk = N{z}{r}.getM();
                if(r>1) %non sono nel bordo
                    rprev = 99-N{z}{r-1}.getX();
                    psiprev = N{z}{r-1}.getPsi();
                    Mprev = N{z}{r-1}.getM();
                
                else %TOFIX hack hack hack!!
                    rprev = 0; %xb
                    psiprev = 0; %dovrebbe essere 0
                    Mprev = 0;
                end
                
                rm = (rk + rprev)/2;
                
                Fr = @(r) [ (Mprev/(6*dz))*(rk-r)^3 + (Mk/(6*dz))*(r-rprev)^3 + (psik/dz - (Mk*dz)/6)*(r - rprev) + (psiprev/dz - (Mprev*dz)/6)*(rk-r) - flux];
                dFr = @(r) [ (-Mprev/(2*dz))*(rk-r)^2 + (Mk/(2*dz))*(r-rprev)^2 + (psik/dz - (Mk*dz)/6) - (psiprev/dz - (Mprev*dz)/6) ];
    
                [res,~,~] = newton(Fr,dFr,rm,tol,1000);
                
                rr = [rr; [N{z}{r}.getY(),99-res(end)]]; %TOFIX preallocare!!
                
                break;
            end
        end
    end
    
    endTime = cputime - startingTime;
    fprintf('\n --- Linea di flusso completata in %f secondi --- \n', endTime);
end