function [ M, counter ] = init_nodi( dr,dz,xa,xb,ya,yb,yd,xg,yg,rg,xf,yf,rf,xp,yp,xc,yc  )


    fprintf(' --- Inizializzazione nodi ---');
    startingTime = cputime;
    counter = 0;

    fcircle_big_y = @(x) [yg-sqrt(rg^2-(x-xg)^2)];
    fcircle_small_y = @(x) [yf-sqrt(rf^2-(x-xf)^2)];
    fcircle_big_x = @(y) [xg+sqrt(rg^2-(y-yg)^2)];
    fcircle_small_x = @(y) [xf+sqrt(rf^2-(y-yf)^2)];
  
    fline_y = @(x) [(yc-yp)*(x-xp)/(xc-xp) + yp];
    fline_x = @(y) [(xc-xp)*(y-yp)/(yc-yp) + xp];

    l = ceil((xb-xa)/dz) -1; %numero di divisioni in dz
    M = cell(l);
    i = 1;    
    x = xa; %x iniziale
    
    while i <= l
        
        x = x + dz;
        
        l1 = 1; l2 = 1; l3 = 1; l4 = 1;        
        if(x<=xg) %segmento AM
            ymin = ya+dr;
        else %semicerchio MN
            yminc = feval(fcircle_big_y,x);

            %calcolo lambda 4 per il contorno inferiore
            nn = floor(abs(yminc-ya)/dr)+1;
            
            ymin = ya + nn*dr;
           
            l4 = abs(ymin-yminc)/dr;   
            
        end
        
        if (x<=xf) %segmento DO
            ymax = yd;
        elseif (x>xf & x<=xp) %semicerchio OP
            ymax = feval(fcircle_small_y,x);
        elseif (x>xp & x<=xc) %semiretta PC
            ymax = feval(fline_y,x);
        else %segmento CB
            ymax = yc;
        end

        n = ceil((ymax-ymin)/dr);
        %calcolo lambda 2 per il contorno superiore
        l2 = abs(ymax - (ymin+(n-1)*dr))/dr;
       
        M{i} = cell(n);
        
        y = ymin;
        
        dpsi = 1/(n);
        psi = dpsi;
        %psi = 0.5
        %dpsi = 0;
        %sotto
        M{i}{1} = Nodo(x,y,1,1,1,l4,psi,psi,psi+dpsi,psi,0);
        
        %interni
        for j = 2:1:(n-1)
            y = y + dr;   
            psi = psi + dpsi;
            M{i}{j} = Nodo(x,y,1,1,1,1,psi,psi,psi+dpsi,psi,psi-dpsi);
            counter = counter +1;
        end
        
        %sopra
        y = y + dr;
        if(x >= xc)
            M{i}{n} = Nodo(x,y,1,l2,1,1,psi,psi,psi,psi,psi-dpsi);
        else
            M{i}{n} = Nodo(x,y,1,l2,1,1,psi,psi,1,psi,psi-dpsi);
        end
        
        counter = counter +2; %primo e ultimo
           
        %controllo intorni sinistro e destro
        
        %sinistro        
        if (x>=xf & x<=(xc+dz))
            k = n;
            while k >= 1
               %calcolo lamda3 per il contorno superiore sinistro
                yy = M{i}{k}.getY(); 
                if(yy >= yp)
                    %semiretta PC
                    xsx = feval(fline_x,M{i}{k}.getY());
                    l3 = abs(x - xsx)/dz;
                    if(l3<1)
                        M{i}{k}.setLambda3(l3);
                        M{i}{k}.setPsiSx(1);
                        
                    end
                    
                elseif(yy >= yd & yy < yp) %semicerchio OP
                    xsx = feval(fcircle_small_x,M{i}{k}.getY());
                    l3 = abs(x - xsx)/dz;
                    if(l3<1)
                        M{i}{k}.setLambda3(l3);
                        M{i}{k}.setPsiSx(1);
                    else
                        break;
                    end
                end
                k = k - 1;
            end
        end
        
        %destro
        if(x>=xg)
            
            k = 1;
            while k <= n
                if(M{i}{k}.getY() <= yg) %sono nel cerchio
                    %calcolo lamda1 per il contorno inferiore a destra
                
                    xdx = feval(fcircle_big_x,M{i}{k}.getY());
                    l1 = abs(xdx - x)/dz;
                    if(l1<1)
                        M{i}{k}.setLambda1(l1);
                        M{i}{k}.setPsiDx(0);
                    end
                else %sono nel segmento NB
                    %calcolo lamda1 per il contorno inferiore a destra
                    l1 = abs(xb - x)/dz;
                    if(l1<1)
                        M{i}{k}.setLambda1(l1);
                        M{i}{k}.setPsiDx(0);
                    end
                end
                k = k + 1;
            end
        end

        i = i + 1;
    end
    
    endTime = cputime - startingTime;
    fprintf('\n --- Inizializzazione nodi completata in %f secondi --- \n', endTime);
end