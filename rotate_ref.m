function [ final ] = rotate_ref( M,dr,dz,p45 )

    fprintf('\n\t--- Rotazione sistema di riferimento ---\n');
    startingTime = cputime;
    
    n = length(M);
    m = length(M{n});
    ymin = M{1}{1}.getY();
    ymax = M{n}{m}.getY();
    xmin = M{n}{1}.getX();
    xmax = M{p45}{1}.getX();
    
    vy = ymin:dr:ymax;
    vx = xmin:dz:xmax;
    
    vet = cell(length(vy));
    for kk = 1:1:length(vet)
        vet{kk} = cell(n - (p45+1),1);
    end
    
    for i = (p45+1):1:n
        for j = 1:1:length(M{i})
            
            pj = (M{i}{j}.getY()-ymin)/dr;
            pj = round(pj);
            vet{pj}{i-p45} = M{i}{j};
        end
    end
    final = {};
    i = 0;
    %elimino gli elementi vuoti
    for kk = 1:1:length(vet)
        j = 0;
        temp = {};
        hh = length(vet{kk});
        while hh > 0
            if (isa(vet{kk}{hh},'Nodo'))
                j = j + 1;
                temp{j} = vet{kk}{hh};
            end
            hh = hh -1;
        end
        if(length(temp) > 0)
            i = i + 1;
            final{i} = temp;
        end
    end
    
    endTime = cputime - startingTime;
    fprintf('\t--- Rotazione completata in %f secondi ---', endTime);

end