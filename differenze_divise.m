function [ M ] = differenze_divise( dr,dz,ya,tol,M,nodi_tot,xc )

    startingTime = cputime;
    
    itmax = ((100)/dz) * ((100)/dr) * 10;
    iter = 0;
    
    fprintf(' --- Inizio calcolo di %.0f nodi. Iterazioni max: %.0f ---',nodi_tot,itmax);

    %f=@(f1,f2,f3,f4,dz,dr) ( ((dr)^2*(dz)^2) * ( (f1+f3)/(dr)^2-(f2-f4)/(2*r0*dr^2) ) )/(2*(dr^2+dz^2));
    if(dr == dz)
        f=@(f1,f2,f3,f4,l1,l2,l3,l4,d,dd,r0) ( ((l1*l2*l3*l4)/(l1*l3+l2*l4))*(((f1/l1 + f3/l3)/(l1+l3))+((f2/l2 + f4/l4)/(l2+l4))-((d*(f2-f4))/(2*r0*(l2+l4)))) );
    else
        f=@(f1,f2,f3,f4,l1,l2,l3,l4,dz,dr,r0) ( ( ( ((f2/l2 + f4/l4)/((l2+l4)*dr^2)) + ((f1/l1 + f3/l3)/((l1+l3)*dz^2)) ) - ( (f2-f4)/(2*r0*dr*(l2+l4)) ) ) / ( (l1*l3*dz^2 + l2*l4*dr^2)/(l1*l2*l3*l4*dz^2*dr^2) ));
    end
    continua = true;
    counter = 0; perc = 0;
    while continua && iter < itmax
        if(not((counter/nodi_tot)*100 <= perc))
            perc = (counter/nodi_tot)*100;
            fprintf('\n Completamento: %3.2f%%',perc);
        end
        counter = 0;
        
        continua = false;
        n = length(M);
        for x=1:1:n
            m = length(M{x});
            for y=1:1:m
                
                vl = M{x}{y}.getLambda();
                
                r0 = M{x}{y}.getY();
                psi = feval(f,M{x}{y}.getPsiDx(),M{x}{y}.getPsiUp(),M{x}{y}.getPsiSx(),M{x}{y}.getPsiDown(),vl(1),vl(2),vl(3),vl(4),dz,dr,r0); 
                M{x}{y}.setPsi(psi);
                
                if(not(M{x}{y}.isReady(tol)))
                    continua = true;
                else
                    counter = counter + 1;
                end
                vprev = {}; vnext = {};
                if(x > 1)
                   vprev = M{x-1};
                end
                if(x < n)
                    vnext = M{x+1};
                end
                [yyprev,yynext] = check_y(vprev,M{x}{y}.getY(),vnext,y);
                
                %aggiorno le informazioni ai nodi vicini
                if x > 1
                    %[yy,temp] = check_y(M{x-1},M{x}{y}.getY(),{},y);
                    if(yyprev > 0 && isa(M{x-1}{yyprev},'Nodo'))
                        M{x-1}{yyprev}.setPsiDx(psi);
                    else
                        M{x}{y}.setPsiSx(1); %condizioni di dirichelet nel bordo superiore
                    end
                else
                     M{x}{y}.setPsiSx(psi); %condizioni di newman in AD
                end
                %[temp,yy] = check_y({},M{x}{y}.getY(),M{x+1},y);
                if x < n && yynext > 0 && isa(M{x+1}{yynext},'Nodo')
                    
                    M{x+1}{yynext}.setPsiSx(psi);
                else
                    M{x}{y}.setPsiDx(0); %condizioni Dirichelet bordo in basso
                end
                
                if y > 1
                    M{x}{y-1}.setPsiUp(psi);
                else
                    M{x}{y}.setPsiDown(0);
                end
                
                if y < m
                    M{x}{y+1}.setPsiDown(psi);
                else
                    if M{x}{y}.getX() >= xc
                        M{x}{y}.setPsiUp(psi); %condizioni di Newmann in uscita
                    else
                        M{x}{y}.setPsiUp(1); %condizioni Dirichelet bordo superiore
                    end
                end
            end
        end
        iter = iter +1;
    end
    
    endTime = cputime - startingTime;
    fprintf('\n --- Calcolo completato in %5.2f secondi Iterazioni: %d ---', endTime,iter);
end