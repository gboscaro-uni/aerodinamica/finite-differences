%coordinate punti notevoli
xa=10; ya=7;
xb=99; yb=100;
xc=89; yc=100;
xd=10; yd=39.5;
xg=51.5; yg=54.5; rg=47.5;
xf=64; yf=59.5; rf=20;

figure(1);
%assi
xmax=xb+10; ymax=yb+10;
xmin=xa-10; ymin=ya-10;
axis([xmin xmax ymin ymax]); hold on

%nomi punti
plot(xa, ya, 'ko'); text(xa-5,ya-3,'A');
plot(xb, yb, 'ko'); text(xb+3,yb+3,'B');
plot(xc, yc, 'ko'); text(xc-5,yc+3,'C');
plot(xd, yd, 'ko'); text(xd-5,yd+3,'D');
plot(xf, yf-rf, 'ko'); text(xf-5,yf-rf+3,'O');

plot(xg, yg-rg, 'ko'); text(xg+3,yg-rg-3,'M');
plot(xg+rg, yg, 'ko'); text(xg+rg+3,yg-3,'N');

%segmenti
xAD=[xa,xd]; yAD=[ya,yd]; xBC=[xb,xc]; yBC=[yb,yc];
xAM=[xa,xg]; yAM=[ya,yg-rg]; xBM=[xb,xg+rg]; yBM=[yb,yg];
xDO=[xd,xf]; yDO=[yd,yf-rf];

plot(xAD,yAD,'r-');
plot(xBC,yBC,'r-');
plot(xAM,yAM,'r-');
plot(xBM,yBM,'r-');
plot(xDO,yDO,'r-');

CF=sqrt((xc-xf)^2+(yc-yf)^2);
delta=(xf*(CF^2-rf^2)+xc*rf^2)^2-(CF^2)*((xf^2-rf^2)*(CF^2-rf^2)+xc^2*rf^2);
xP1=(xf*(CF^2-rf^2)+xc*rf^2)/(CF^2)+(sqrt(delta))/(CF^2);
xP2=(xf*(CF^2-rf^2)+xc*rf^2)/(CF^2)-(sqrt(delta))/(CF^2);

if xP1>xf
    
    xP=xP1;
    
elseif xP2>xf
    xP=xP2;
end

yP1=yf+sqrt(rf^2-(xP-xf)^2);
yP2=yf-sqrt(rf^2-(xP-xf)^2);

if yP1<yf
    
    yP=yP1;    
elseif yP2<yf
    
    yP=yP2;    
end

plot(xP, yP, 'ko'); text(xP-5,yP+3,'P');
xCP=[xc,xP];
yCP=[yc,yP];
plot(xCP,yCP,'r-');
plot(xg, yg, 'k*'); text(xg-5,yg+3,'G');
plot(xf, yf, 'k*'); text(xf-5,yf+3,'F');

% circonferenze
a=3*pi/2; b=0.0001;
P=draw_circle(a,b,xg,yg,rg,2*pi);
plot(P(:,1),P(:,2),'r-');

a=3*pi/2; b=0.0001; t=a+atan((xP-xf)/(yf-yP));
P=draw_circle(a,b,xf,yf,rf,t);
plot(P(:,1),P(:,2),'r-');

hold off;











