classdef Nodo < handle
   properties (SetAccess = 'private', GetAccess = 'public')
      x; y;
      lambda1; lambda2; lambda3; lambda4;
      psi;
      psi1; psi2; psi3; psi4;
      prev;
      M;
   end   

   methods (Access = public)
      function obj = Nodo(x,y,lambda1,lambda2,lambda3,lambda4,p,p1,p2,p3,p4)
          obj.psi = p;
          obj.psi1 = p1;
          obj.psi2 = p2;
          obj.psi3 = p3;
          obj.psi4 = p4;

          obj.lambda1 = lambda1;
          obj.lambda2 = lambda2;
          obj.lambda3 = lambda3;
          obj.lambda4 = lambda4;
          
          obj.x = x;
          obj.y = y;
          
          obj.prev = -1;
          
          M = 0;
      end
      
      function r = isInside(obj)
         r = obj.lambda1 == 1 & obj.lambda2 == 1 & obj.lambda2 == 1 & obj.lambda2 == 1;
      end
      function r = isReady(obj,tol)
         r = abs(obj.prev - obj.psi) < tol;
      end
   end
              
   %getters/setters
   methods
      %M GETTER
      function M = getM(obj)
         M = obj.M;
      end
      %M SETTER
      function setM(obj,M)
         obj.M = M;
      end
      %coordinate GETTER
      function x = getX(obj)
         x = obj.x;
      end
      function y = getY(obj)
         y = obj.y;
      end 
      %lambda GETTER
      function v = getLambda(obj)
         v = [obj.lambda1,obj.lambda2,obj.lambda3,obj.lambda4];
      end    
      %lambda SETTER
      function setLambda1(obj,l)
        if(l<=0)
            error('Lambda value must be positive');
        else
            obj.lambda1 = l;
        end
      end
      function setLambda2(obj,l)
        if(l<=0)
            error('Lambda value must be positive');
        else
            obj.lambda2 = l;
        end
      end
      function setLambda3(obj,l)
        if(l<=0)
            error('Lambda value must be positive');
        else
            obj.lambda3 = l;
        end
      end
      function setLambda4(obj,l)
        if(l<=0)
            error('Lambda value must be positive');
        else
            obj.lambda4 = l;
        end
      end
      %prev GETTER
      function r = getPrev(obj)
         r = obj.prev;
      end  
      %psi GETTER
      function r = getPsi(obj)
         r = obj.psi;
      end 
      function r = getPsiDx(obj)
         r = obj.psi1;
      end 
      function r = getPsiUp(obj)
         r = obj.psi2;
      end 
      function r = getPsiSx(obj)
         r = obj.psi3;
      end 
      function r = getPsiDown(obj)
         r = obj.psi4;
      end 
      %psi SETTER
      function setPsi(obj,p)
         obj.prev = obj.psi;
         obj.psi = p;
      end
      function setPsiDx(obj,p)
         obj.psi1 = p;
      end 
      function setPsiUp(obj,p)
         obj.psi2 = p;
      end 
      function setPsiSx(obj,p)
         obj.psi3 = p;
      end 
      function setPsiDown(obj,p)
         obj.psi4 = p;
      end 
   end
end